import React, {Component} from 'react';
import './App.css';
import {Route, Switch} from "react-router-dom";
import Layout from "./components/Layout/Layout";
import Page from "./components/Page/Page";

class App extends Component {
  render() {
    return (
      <div className="wrapper">
        <Layout>
          <Switch>
            <Route path="/" exact component={Page}/>
            <Route path="/pages/:content" component={Page}/>
            <Route path="/pages/admin" exact/>
            <Route render={() => <h1>Page not found</h1>}/>
          </Switch>
        </Layout>
      </div>
    );
  }
}

export default App;
