import React, {Fragment} from 'react';
import Toolbar from "../Navigation/Toolbar/Toolbar";
import './Layout.css';

const Layout = props => {
  return (
      <Fragment>
        <Toolbar/>
        <main className="content-box">
          <div className="container">
            {props.children}
          </div>
        </main>
      </Fragment>
  );
};

export default Layout;


