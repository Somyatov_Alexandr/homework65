import React from 'react';
import './Logo.css';
import ImgLogo from '../../../assets/img/logo.png';

const Logo = () => {
  return (
      <div className="logo">
        <img src={ImgLogo} alt="firebase"/>
      </div>
  );
};

export default Logo;
