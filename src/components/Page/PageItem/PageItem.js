import React, {Fragment} from 'react';

const PageItem = props => {
  return (
      <Fragment>
        <h1 className="page__heading">{props.title}</h1>
        <div className="page__text">{props.content}</div>
      </Fragment>
  );
};

export default PageItem;
