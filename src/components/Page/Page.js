import React, {Component} from 'react';
import axios from "axios";
import PageItem from "./PageItem/PageItem";

class Page extends Component {
  state = {
    page: {},
    loading: false
  };

  getContent () {
    let pageName = this.props.match.params.content;
    if (!pageName) {
      pageName = 'home';
    }
    this.setState({loading: true});
    // pageName = !pageName ? 'home' : null;
    axios.get(`/pages/${pageName}.json`).then(response => {
      if (response.data) {
        this.setState({page: response.data, loading: false})
      }
    }).catch((error) => {
      this.setState({loading: false});
      console.log(error);
    })
  }

  componentDidMount () {
    this.getContent()
  }

  componentDidUpdate (prevProps) {
    const pageName = this.props.match.params.content;
    if (pageName !== prevProps.match.params.content){
      this.getContent();
    }
  }

  render() {
    return (
      <PageItem title={this.state.page.title} content={this.state.page.content}/>
    );
  }
}

export default Page;
