import React from 'react';
import {NavLink} from "react-router-dom";

const NavItem = props => (
    <li className="nav__item">
      <NavLink className="nav__link" to={props.to} exact={props.exact}>
        {props.children}
      </NavLink>
    </li>
);

export default NavItem;