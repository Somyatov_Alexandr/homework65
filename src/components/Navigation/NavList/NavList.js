import React from 'react';
import NavItem from "./NavItem/NavItem";

const NavList = () => (
    <menu className="nav__list">
      <NavItem to="/" exact>Home</NavItem>
      <NavItem to="/pages/about" exact>About</NavItem>
      <NavItem to="/pages/news" exact>News</NavItem>
      <NavItem to="/pages/lessons" exact>Lessons</NavItem>
      <NavItem to="/pages/contact" exact>Contact</NavItem>
    </menu>
);

export default NavList;