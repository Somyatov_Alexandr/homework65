import React from 'react';
import NavList from "../NavList/NavList";
import Logo from "../../UI/Logo/Logo";
import './Toolbar.css';

const Toolbar = () => (
    <header className="header">
      <div className="container">
        <Logo/>
        <nav className="top-nav">
          <NavList/>
        </nav>
      </div>
    </header>
);

export default Toolbar;